/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.programxo;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class ProgramXO {

    static Scanner sc = new Scanner(System.in);
    static char player1 = 'X';
    static char player2 = 'O';
    static char currentPlayer;
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static int row = 0;
    static int col = 0;

    public static void main(String[] args) {
        printWelcome();
        printChooseCurrentPlayer();
        inputCurrentPlayer();
        while(true){
            printTable();
            printTurn();
            inputRowCol();
            if (isWinner()) {
                printTable();
                ptintWin();
                break;
            }
            if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
        inputContinue();

    }

    private static void printWelcome() {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
    }

    private static void printChooseCurrentPlayer() {
        System.out.println("|---------------------|");
        System.out.println("| Choose who go first |");
        System.out.println("|    Press 1 is X     |");
        System.out.println("|    Press 2 is O     |");
        System.out.println("|---------------------|");

    }

    private static void inputCurrentPlayer() {
        System.out.print("Choose here : ");

        while (true) {
            currentPlayer = sc.next().charAt(0);
            if (currentPlayer == '1') {
                currentPlayer = player1;
                System.out.println("X go first");
                break;
            } else if (currentPlayer == '2') {
                currentPlayer = player2;
                System.out.println("O go first");
                break;
            } else {
                System.out.print("Choose again : ");
            }
        }

    }

    private static void printTable() {
     System.out.println("|-----------|");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();
            System.out.println("|-----------|");
        }

    }

    private static void printTurn() {
        if (currentPlayer == 'X') {
            System.out.println("It's player " + currentPlayer + " turn. Please input row[1-3] and column[1-3] for your move");
        } else {
            System.out.println("It's player " + currentPlayer + " turn. Please input row[1-3] and column[1-3] for your move");
        }
    }
    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
            System.out.println("Invalid move. Please try again.");
        }
    }    

    private static void switchPlayer() {
        currentPlayer = (currentPlayer == player1) ? player2 : player1;
    }
    
    private static boolean isWinner() {
        if (checkRow() | checkCol() | checkX1() | checkX2()) {
            return true;
        }
        return false;
    }

    private static void ptintWin() {
        System.out.println("Congratulations! Player " + currentPlayer + " wins!");
    }
    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }
    private static boolean isDraw() {
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    private static void printDraw() {
        System.out.println("The result is Draw!");
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;

    }
    private static void inputContinue() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Do you want to play again? (y/n): ");
            String newGame = kb.next();
            if (newGame.equalsIgnoreCase("y")) {
                resetGame();
                main(null);
                break;
            } else if (newGame.equalsIgnoreCase("n")) {
                System.out.println("Thank you for playing! Goodbye!");
                break;
            } else {
                System.out.println("Invalid input. Please enter 'y' or 'n'.");
            }
        }
    }

    private static void resetGame() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = 'X';
    }

}
